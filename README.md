Updashd Worker PHP - HTTP/HTTPS
===============================

Sample Config
-------------
    {
         "ip": "192.168.1.252",
         "hostname": "anotherhost",
         "prefer_ip": false,
         "port": 80,
         "timeout": "anotherhost",
         "path": "/home",
         "method": "GET",
         "referer": "/referer",
         "user_agent": "/referer",
         "max_redirects": 3,
         "headers": {
             "Host": "testing.updashd.com"
         }
    }