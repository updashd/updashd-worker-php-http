<?php
require __DIR__ . '/../vendor/autoload.php';

// HTTP GET
$config = \Updashd\Worker\CurlHttp::createConfig();

//foreach ($config->getFields() as $field) {
//    echo $field->getLabel() . PHP_EOL;
//}

foreach ($config->getGroups() as $group) {
    echo $group->getLabel() . PHP_EOL;

    foreach ($group->getFields() as $field) {
        echo '    ' . $field->getLabel() . PHP_EOL;
    }
}