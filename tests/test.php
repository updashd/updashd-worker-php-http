<?php
require __DIR__ . '/../vendor/autoload.php';

// HTTP GET
$config = \Updashd\Worker\CurlHttp::createConfig();
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_HOSTNAME, 'docs.updashd.com');
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_PATH, '/Main_Page');
$httpWorker = new \Updashd\Worker\CurlHttp($config);
$result = $httpWorker->run();

print_r($result);

// HTTPS GET
$config = \Updashd\Worker\CurlHttps::createConfig();
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_HOSTNAME, 'docs.updashd.com');
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_PATH, '/Main_Page');
$httpsWorker = new \Updashd\Worker\CurlHttps($config);
$result = $httpsWorker->run();

print_r($result);

// HTTPS POST
$config = \Updashd\Worker\CurlHttps::createConfig();
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_HOSTNAME, 'test.updashd.com');
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_PATH, '/auth/signin');
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_METHOD, 'POST');
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_POST_BODY, 'username=test&password=1234');
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_RECORD_RESPONSE_HEADERS_FOR, ['SUCCESS', 'CRITICAL']);
$config->setValue(\Updashd\Worker\CurlHttp::FIELD_RECORD_RESPONSE_FOR, ['SUCCESS', 'CRITICAL']);
$httpsPostWorker = new \Updashd\Worker\CurlHttps($config);
$result = $httpsPostWorker->run();

print_r($result);