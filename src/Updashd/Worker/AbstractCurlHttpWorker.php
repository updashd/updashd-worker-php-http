<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;
use Updashd\Configlib\Validator\HostnameValidator;
use Updashd\Configlib\Validator\Ip4Validator;
use Updashd\Configlib\Validator\NumberValidator;
use Updashd\Configlib\Validator\PortNumberValidator;
use Updashd\Worker\Exception\WorkerConfigurationException;

abstract class AbstractCurlHttpWorker implements WorkerInterface {
    const FIELD_REFERER = 'referer';
    const FIELD_TIMEOUT = 'timeout';
    const FIELD_USER_AGENT = 'user_agent';
    const FIELD_MAX_REDIRECTS = 'max_redirects';
    const FIELD_PREFER_IP = 'prefer_ip';
    const FIELD_METHOD = 'method';
    const FIELD_HOSTNAME = 'hostname';
    const FIELD_IP = 'ip';
    const FIELD_PATH = 'path';
    const FIELD_PORT = 'port';
    const FIELD_HEADERS = 'headers';
    const FIELD_PROTOCOL = 'protocol';
    const FIELD_SSL_VERIFY_PEER = 'ssl_verify_peer';

    const FIELD_CONTENT_TYPE = 'content_type';
    const FIELD_POST_BODY = 'post_body';

    const FIELD_CHECK_HTTP_CODE = 'check_http_code';
    const FIELD_CORRECT_HTTP_CODE = 'correct_http_code';
    const FIELD_WRONG_HTTP_CODE_RESULT_STATUS = 'wrong_http_code_result_status';

    const FIELD_CHECK_RESPONSE_SIZE = 'check_response_size';
    const FIELD_CORRECT_RESPONSE_SIZE = 'correct_response_size';
    const FIELD_WRONG_RESPONSE_SIZE_RESULT_STATUS = 'wrong_response_size_result_status';

    const FIELD_RECORD_RESPONSE_FOR = 'record_response_for';
    const FIELD_RECORD_RESPONSE_HEADERS_FOR = 'record_response_headers_for';

    const FIELD_AUTH_METHOD = 'auth_method';

    const FIELD_AUTH_USERNAME = 'auth_username';
    const FIELD_AUTH_PASSWORD = 'auth_password';

    const FIELD_OAUTH_TOKEN_URL = 'oauth_token_url';
    const FIELD_OAUTH_CLIENT_ID = 'oauth_client_id';
    const FIELD_OAUTH_CLIENT_SECRET = 'oauth_client_secret';

    const GROUP_BASIC = 'basic';
    const GROUP_NETWORK = 'network';
    const GROUP_HTTP_OPTIONS = 'http_options';
    const GROUP_PARAMETERS = 'parameters';
    const GROUP_STATUS_CODE_CHECK = 'status_code_check';
    const GROUP_RESPONSE_SIZE_CHECK = 'response_size_check';
    const GROUP_METRICS = 'metrics';
    const GROUP_AUTHENTICATION = 'auth';

    const AUTH_NONE = 'none';
    const AUTH_BASIC = 'basic';
    const AUTH_OAUTH_2 = 'oauth2';

    const METRIC_REQUESTED_URL = 'requested_url';
    const METRIC_CONNECT_TIME = 'connect_time';
    const METRIC_RESPONSE_TIME = 'response_time';
    const METRIC_NAMELOOKUP_TIME = 'namelookup_time';
    const METRIC_PRETRANSFER_TIME = 'pretransfer_time';
    const METRIC_HTTP_CODE = 'http_code';
    const METRIC_RESPONSE_SIZE = 'response_size';
    const METRIC_RESPONSE = 'response';
    const METRIC_RESPONSE_HEADERS = 'response_headers';

    private $config;
    private $curl;

    public static function createConfig () {
        $config = new Config();

        $config->addFieldText(self::FIELD_HOSTNAME, 'Host Name', null, true)
            ->setReferenceField('hostname')
            ->addValidator(new HostnameValidator());

        $config->addFieldText(self::FIELD_IP, 'IP')
            ->setReferenceField('ip')
            ->addValidator(new Ip4Validator());

        $config->addFieldCheckbox(self::FIELD_PREFER_IP, 'Prefer IP', false);

        $config->addFieldSelect(self::FIELD_PROTOCOL, 'Protocol', [
            'http' => 'HTTP',
            'https' => 'HTTPS',
        ], 'http');

        $config->addFieldNumber(self::FIELD_PORT, 'Port')
            ->addValidator(new PortNumberValidator());

        $config->addFieldText(self::FIELD_PATH, 'Path', '/');

        $config->addFieldSelect(self::FIELD_METHOD, 'Method', [
            'GET' => 'GET',
            'POST' => 'POST',
        ], 'GET');

        $config->addFieldNumber(self::FIELD_TIMEOUT, 'Time Out', 30)
            ->addValidator(new NumberValidator(false, 1, 30));

        $config->addFieldText(self::FIELD_REFERER, 'Referer', '');

        $config->addFieldText(self::FIELD_USER_AGENT, 'User Agent', 'Updashd Server Monitoring');

        $config->addFieldNumber(self::FIELD_MAX_REDIRECTS, 'Max Redirects', 3)
            ->addValidator(new NumberValidator(false, 1, 10));

        $config->addFieldCheckbox(self::FIELD_SSL_VERIFY_PEER, 'SSL Verify Peer', true);

        $config->addFieldMultiLineText(self::FIELD_HEADERS, 'Headers');

        $config->addFieldSelect(self::FIELD_CONTENT_TYPE, 'POST Content-Type',
            self::getContentTypeOptions(), 'application/x-www-form-urlencoded');

        $config->addFieldMultiLineText(self::FIELD_POST_BODY, 'POST Body');

        $config->addFieldCheckbox(self::FIELD_CHECK_HTTP_CODE, 'Check HTTP Status Code', true);

        $config->addFieldNumber(self::FIELD_CORRECT_HTTP_CODE, 'Correct HTTP Status Code', 200);

        $config->addFieldSelect(self::FIELD_WRONG_HTTP_CODE_RESULT_STATUS, 'Wrong HTTP Status Result Status',
            self::getResultStatusOptions(), Result::STATUS_WARNING);

        $config->addFieldCheckbox(self::FIELD_CHECK_RESPONSE_SIZE, 'Check Response Size', false);

        $config->addFieldNumber(self::FIELD_CORRECT_RESPONSE_SIZE, 'Correct Response Size');

        $config->addFieldSelect(self::FIELD_WRONG_RESPONSE_SIZE_RESULT_STATUS, 'Wrong Response Size Status',
            self::getResultStatusOptions(), Result::STATUS_WARNING);

        $config->addFieldMultiCheckbox(self::FIELD_RECORD_RESPONSE_FOR, 'Record Response For',
            self::getRecordResponseForOptions(), self::getRecordResponseForDefaults());

        $config->addFieldMultiCheckbox(self::FIELD_RECORD_RESPONSE_HEADERS_FOR, 'Record Response Headers For',
            self::getRecordResponseForOptions(), self::getRecordResponseForDefaults());

        $config->addFieldSelect(self::FIELD_AUTH_METHOD, 'Method', [
            self::AUTH_NONE => 'No Auth',
            self::AUTH_BASIC => 'Basic Auth',
            // 'digest' => 'Digest Auth',
            // 'oauth1' => 'OAuth 1.0',
            self::AUTH_OAUTH_2 => 'OAuth 2.0',
            // 'bearer' => 'Bearer Token',
        ], self::AUTH_NONE, true);

        $config->addFieldText(self::FIELD_AUTH_USERNAME, 'Username');
        $config->addFieldText(self::FIELD_AUTH_PASSWORD, 'Password');

        $config->addFieldUrl(self::FIELD_OAUTH_TOKEN_URL, 'Token URL');
        $config->addFieldText(self::FIELD_OAUTH_CLIENT_ID, 'Client ID');
        $config->addFieldText(self::FIELD_OAUTH_CLIENT_SECRET, 'Client Secret');

        ///////////////////
        // Configure Groups
        ///////////////////

        // Basic
        $config->addGroup(self::GROUP_BASIC, 'Basic');
        $config->addFieldToGroup(self::GROUP_BASIC, self::FIELD_PATH);

        // Network
        $config->addGroup(self::GROUP_NETWORK, 'Network');
        $config->addFieldToGroup(self::GROUP_NETWORK, self::FIELD_HOSTNAME);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::FIELD_IP);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::FIELD_PREFER_IP);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::FIELD_PORT);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::FIELD_TIMEOUT);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::FIELD_SSL_VERIFY_PEER);

        // HTTP Options
        $config->addGroup(self::GROUP_HTTP_OPTIONS, 'HTTP Options');
        $config->addFieldToGroup(self::GROUP_HTTP_OPTIONS, self::FIELD_PROTOCOL);
        $config->addFieldToGroup(self::GROUP_HTTP_OPTIONS, self::FIELD_METHOD);
        $config->addFieldToGroup(self::GROUP_HTTP_OPTIONS, self::FIELD_REFERER);
        $config->addFieldToGroup(self::GROUP_HTTP_OPTIONS, self::FIELD_USER_AGENT);
        $config->addFieldToGroup(self::GROUP_HTTP_OPTIONS, self::FIELD_MAX_REDIRECTS);
        $config->addFieldToGroup(self::GROUP_HTTP_OPTIONS, self::FIELD_HEADERS);

        // Parameters
        $config->addGroup(self::GROUP_PARAMETERS, 'Parameters');
        $config->addFieldToGroup(self::GROUP_PARAMETERS, self::FIELD_CONTENT_TYPE);
        $config->addFieldToGroup(self::GROUP_PARAMETERS, self::FIELD_POST_BODY);

        // HTTP Status Code Check
        $config->addGroup(self::GROUP_STATUS_CODE_CHECK, 'HTTP Status Code Check');
        $config->addFieldToGroup(self::GROUP_STATUS_CODE_CHECK, self::FIELD_CHECK_HTTP_CODE);
        $config->addFieldToGroup(self::GROUP_STATUS_CODE_CHECK, self::FIELD_CORRECT_HTTP_CODE);
        $config->addFieldToGroup(self::GROUP_STATUS_CODE_CHECK, self::FIELD_WRONG_HTTP_CODE_RESULT_STATUS);

        // HTTP Response Size Check
        $config->addGroup(self::GROUP_RESPONSE_SIZE_CHECK, 'Response Size Check');
        $config->addFieldToGroup(self::GROUP_RESPONSE_SIZE_CHECK, self::FIELD_CHECK_RESPONSE_SIZE);
        $config->addFieldToGroup(self::GROUP_RESPONSE_SIZE_CHECK, self::FIELD_CORRECT_RESPONSE_SIZE);
        $config->addFieldToGroup(self::GROUP_RESPONSE_SIZE_CHECK, self::FIELD_WRONG_RESPONSE_SIZE_RESULT_STATUS);

        // Metrics to Keep
        $config->addGroup(self::GROUP_METRICS, 'Metrics');
        $config->addFieldToGroup(self::GROUP_METRICS, self::FIELD_RECORD_RESPONSE_FOR);
        $config->addFieldToGroup(self::GROUP_METRICS, self::FIELD_RECORD_RESPONSE_HEADERS_FOR);

        // Authentication
        $config->addGroup(self::GROUP_AUTHENTICATION, 'Authentication');
        $config->addFieldToGroup(self::GROUP_AUTHENTICATION, self::FIELD_AUTH_METHOD);
        $config->addFieldToGroup(self::GROUP_AUTHENTICATION, self::FIELD_AUTH_USERNAME);
        $config->addFieldToGroup(self::GROUP_AUTHENTICATION, self::FIELD_AUTH_PASSWORD);
        $config->addFieldToGroup(self::GROUP_AUTHENTICATION, self::FIELD_OAUTH_TOKEN_URL);
        $config->addFieldToGroup(self::GROUP_AUTHENTICATION, self::FIELD_OAUTH_CLIENT_ID);
        $config->addFieldToGroup(self::GROUP_AUTHENTICATION, self::FIELD_OAUTH_CLIENT_SECRET);

        return $config;
    }

    public static function createResult () {
        $result = new Result();

        $result->addMetricString(self::METRIC_REQUESTED_URL, 'Requested URL');
        $result->addMetricFloat(self::METRIC_CONNECT_TIME, 'Connect Time', 'sec');
        $result->addMetricFloat(self::METRIC_RESPONSE_TIME, 'Response Time', 'sec');
        $result->addMetricFloat(self::METRIC_NAMELOOKUP_TIME, 'Name Lookup Time', 'sec');
        $result->addMetricFloat(self::METRIC_PRETRANSFER_TIME, 'Pre-Transfer Time', 'sec');
        $result->addMetricInt(self::METRIC_HTTP_CODE, 'HTTP Code');
        $result->addMetricInt(self::METRIC_RESPONSE_SIZE, 'Response Size', 'byte');
        $result->addMetricText(self::METRIC_RESPONSE, 'Response');
        $result->addMetricText(self::METRIC_RESPONSE_HEADERS, 'Response Headers');

        return $result;
    }

    public static function getResultStatusOptions () {
        return [
            Result::STATUS_SUCCESS => 'Success',
            Result::STATUS_NOTICE => 'Notice',
            Result::STATUS_WARNING => 'Warning',
            Result::STATUS_CRITICAL => 'Critical'
        ];
    }

    public static function getRecordResponseForOptions () {
        return [
            Result::STATUS_SUCCESS => 'Success',
            Result::STATUS_NOTICE => 'Notice',
            Result::STATUS_WARNING => 'Warning',
            Result::STATUS_CRITICAL => 'Critical'
        ];
    }

    public static function getRecordResponseForDefaults () {
        return [
            Result::STATUS_CRITICAL => 'Critical'
        ];
    }

    /**
     * Create a worker instance for the given service
     *
     * @param Config $config the json_decoded configuration
     * @throws \Exception
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public function __construct (Config $config) {
        $this->setConfig($config);

        $curl = curl_init();

        // Set the URL
        curl_setopt($curl, CURLOPT_URL, $this->calculateUrl());

        $this->setOptionalCurlOptFromField($curl, CURLOPT_REFERER, self::FIELD_REFERER);
        $this->setOptionalCurlOptFromField($curl, CURLOPT_TIMEOUT, self::FIELD_TIMEOUT);
        $this->setOptionalCurlOptFromField($curl, CURLOPT_USERAGENT, self::FIELD_USER_AGENT);
        $this->setOptionalCurlOptFromField($curl, CURLOPT_MAXREDIRS, self::FIELD_MAX_REDIRECTS);
        $this->setOptionalCurlOptFromField($curl, CURLOPT_SSL_VERIFYPEER, self::FIELD_SSL_VERIFY_PEER);

        if ($this->getConfigValue(self::FIELD_AUTH_METHOD) == self::AUTH_BASIC) {
            $username = $this->getConfigValue(self::FIELD_AUTH_USERNAME);
            $password = $this->getConfigValue(self::FIELD_AUTH_PASSWORD);
            curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        }

        if ($this->getConfigValue(self::FIELD_METHOD) == 'POST') {
            $body = $this->getConfigValue(self::FIELD_POST_BODY, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        }

        // Have curl_exec return the result (this keeps it from being printed)
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // This allows us to extract the headers
        curl_setopt($curl, CURLOPT_HEADER, 1);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->calculateHeaders());

        $this->setCurl($curl);
    }

    private function setOptionalCurlOptFromField ($curl, $curlOpt, $field) {
        $value = $this->getConfigValue($field);

        if ($value) {
            curl_setopt($curl, $curlOpt, $value);
        }
    }

    private function calculateUrl () {
        $host = $this->getConfigValue(self::FIELD_PREFER_IP)
                ? $this->getConfigValue(self::FIELD_IP, true)
                : $this->getConfigValue(self::FIELD_HOSTNAME, true);

        $protocol = $this->getConfigValue(self::FIELD_PROTOCOL);
        $port = $this->getConfigValue(self::FIELD_PORT);
        $url = $protocol;
        $url .= '://';
        $url .= $host;

        if (
            ($protocol == 'http' && $port != 80) ||
            ($protocol == 'https' && $port != 443)
        ) {
            $url .= ':';
            $url .= $port;
        }

        $url .= $this->getConfigValue(self::FIELD_PATH);

        return $url;
    }

    private function calculateHeaders () {
        $assocHeaders = array();

        // Always specify the host header
        $assocHeaders['Host'] = $this->getConfigValue(self::FIELD_HOSTNAME, true);

        if ($this->getConfigValue(self::FIELD_METHOD) == 'POST') {
            $assocHeaders['Content-Type'] = $this->getConfigValue(self::FIELD_CONTENT_TYPE, true);
        }

        if ($this->getConfigValue(self::FIELD_AUTH_METHOD) == self::AUTH_OAUTH_2) {
            $assocHeaders['Authorization'] = 'Bearer ' . $this->getOauth2AccessToken();
        }

        $customHeaders = $this->getConfigValue(self::FIELD_HEADERS);

        if ($customHeaders) {
            $customHeaders = trim($customHeaders);

            $headerLines = preg_split("/\\r\\n|\\r|\\n/", $customHeaders);

            if (is_array($headerLines) && count($headerLines)) {
                foreach ($headerLines as $headerLine) {
                    $headerFields = explode(':', $headerLine);

                    if (count($headerFields) != 2) {
                        throw new WorkerConfigurationException('Invalid header: ' . $headerLine);
                    }

                    list($headerName, $headerValue) = $headerFields;

                    $assocHeaders[$headerName] = $headerValue;
                }
            }
        }

        $finalHeaderLines = array();

        foreach ($assocHeaders as $headerName => $headerValue) {
            $finalHeaderLines[] = $headerName . ': ' . $headerValue;
        }

        return $finalHeaderLines;
    }

    private static function getContentTypeOptions () {
        return [
            'application/x-www-form-urlencoded' => 'application/x-www-form-urlencoded',
            'multipart/form-data' => 'multipart/form-data',
            'application/json' => 'application/json',
            'application/xml' => 'application/xml',
        ];
    }

    public function __destruct () {
        if ($this->getCurl()) {
            curl_close($this->getCurl());
        }
    }

    public function run () {
        $curl = $this->getCurl();

        $response = curl_exec($curl);

        $curlInfo = curl_getinfo($curl);

        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);

        $body = substr($response, $headerSize);

        $httpStatusCode = $curlInfo['http_code'];
        $responseSize = $curlInfo['size_download'];

        $result = self::createResult();

        if ($response === FALSE) {
            $result->setStatus(Result::STATUS_CRITICAL);

            $code = curl_errno($curl);
            $code = $code ?: 0;

            $errorMsg = curl_error($curl);
            $errorMsg = $errorMsg ?: 'No CURL Error Message Available';

            $result->setErrorCode($code);
            $result->setErrorMessage($errorMsg);
        }
        else {
            $result->setStatus(Result::STATUS_SUCCESS);

            // Status Code Check
            if ($this->getConfigValue(self::FIELD_CHECK_HTTP_CODE)) {
                $correctHttpStatusCode = $this->getConfigValue(self::FIELD_CORRECT_HTTP_CODE, true);

                if ($httpStatusCode != $correctHttpStatusCode) {
                    $result->setStatus($this->getConfigValue(self::FIELD_WRONG_HTTP_CODE_RESULT_STATUS));

                    $result->setErrorCode(0);
                    $result->setErrorMessage('HTTP Status Code Mismatch: Desired: ' . $correctHttpStatusCode . ' Actual:' . $httpStatusCode);
                }
            }

            // Response Size Check
            if ($this->getConfigValue(self::FIELD_CHECK_RESPONSE_SIZE)) {
                $correctResponseSize = $this->getConfigValue(self::FIELD_CORRECT_RESPONSE_SIZE, true);

                if ($responseSize != $correctResponseSize) {
                    $result->setStatus($this->getConfigValue(self::FIELD_WRONG_RESPONSE_SIZE_RESULT_STATUS));

                    $result->setErrorCode(0);
                    $result->setErrorMessage('Response Size Mismatch: Desired: ' . $correctResponseSize . ' Actual:' . $responseSize);
                }
            }
        }

        $result->setMetricValue(self::METRIC_REQUESTED_URL, $this->calculateUrl());
        $result->setMetricValue(self::METRIC_CONNECT_TIME, $curlInfo['connect_time']);
        $result->setMetricValue(self::METRIC_RESPONSE_TIME, $curlInfo['total_time']);
        $result->setMetricValue(self::METRIC_NAMELOOKUP_TIME, $curlInfo['namelookup_time']);
        $result->setMetricValue(self::METRIC_PRETRANSFER_TIME, $curlInfo['pretransfer_time']);
        $result->setMetricValue(self::METRIC_HTTP_CODE, $httpStatusCode);
        $result->setMetricValue(self::METRIC_RESPONSE_SIZE, $responseSize);

        // Record response headers when desired
        $recordResponseHeadersStatuses = $this->getConfigValue(self::FIELD_RECORD_RESPONSE_HEADERS_FOR);

        if (in_array($result->getStatus(), $recordResponseHeadersStatuses)) {
            $result->setMetricValue(self::METRIC_RESPONSE_HEADERS, $header);
        }

        // Record response when desired
        $recordResponseStatuses = $this->getConfigValue(self::FIELD_RECORD_RESPONSE_FOR);

        if (in_array($result->getStatus(), $recordResponseStatuses)) {
            $result->setMetricValue(self::METRIC_RESPONSE, $body);
        }

        return $result;
    }

    protected function getOauth2AccessToken () {
        $clientId = $this->getConfigValue(self::FIELD_OAUTH_CLIENT_ID, true);
        $clientSecret = $this->getConfigValue(self::FIELD_OAUTH_CLIENT_SECRET, true);
        $tokenUrl = $this->getConfigValue(self::FIELD_OAUTH_TOKEN_URL, true);
        $oauth2Client = new OAuth2Client($clientId, $clientSecret, $tokenUrl);
        return $oauth2Client->getAccessToken();
    }

    /**
     * @param $key
     * @param bool $required
     * @return null|mixed|string
     * @throws \Exception
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public function getConfigValue ($key, $required = false) {
        $value = $this->getConfig()->getValue($key, true);

        if ($value === null && $required) {
            throw new WorkerConfigurationException('Configuration key missing: "' . $key . '"', 0);
        }

        return $value;
    }

    /**
     * @return Config
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @param Config $config
     */
    public function setConfig (Config $config) {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getCurl () {
        return $this->curl;
    }

    /**
     * @param mixed $curl
     */
    public function setCurl ($curl) {
        $this->curl = $curl;
    }
}