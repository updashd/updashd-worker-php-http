<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;

class CurlHttp extends AbstractCurlHttpWorker {
    
    /**
     * @return Config
     */
    public static function createConfig () {
        $config = parent::createConfig();
        $config->setDefaultValue(parent::FIELD_PORT, 80);
        $config->setDefaultValue(parent::FIELD_PROTOCOL, 'http');
        return $config;
    }
    
    /**
     * Get the name of the service (this should match in the database)
     * @return string
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public static function getServiceName () {
        return 'curl_http';
    }
    
    /**
     * Get the readable name of the service
     */
    public static function getReadableName () {
        return 'Curl HTTP';
    }
}