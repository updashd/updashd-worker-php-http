<?php
namespace Updashd\Worker;

class OAuth2Client {
    protected $clientId;
    protected $clientSecret;
    protected $tokenUrl;

    /**
     * Create a simple Oauth2 Client using the given client id, secret, and the token URL.
     * @param string $clientId The client ID to use
     * @param string $clientSecret The client secret to use
     * @param string $tokenUrl By default, this will use the BYU Oauth2 token url.
     */
    public function __construct ($clientId, $clientSecret, $tokenUrl) {
        $this->setClientId($clientId);
        $this->setClientSecret($clientSecret);
        $this->setTokenUrl($tokenUrl);
    }

    /**
     * Get an access token (or use the one we already have)
     * @param int $validityTime number of seconds to request the token for.
     * @return string
     * @throws \Exception
     */
    public function getAccessToken ($validityTime = 3600) {
        $expiresTimestamp = null;

        $tokenDetails = $this->retrieveToken($validityTime);
        $accessToken = $tokenDetails->access_token;

        // Get the expires timestamp by taking the current time and adding the expires_in seconds to it
        $expiresTimestamp = $tokenDetails->expires_in + time();

        if (! $accessToken) {
            throw new \Exception('Somehow there is no access token at the end of ' . __FUNCTION__ . '!');
        }

        return $accessToken;
    }

    /**
     * @param int $validityTime
     * @return mixed
     * @throws \Exception
     */
    protected function retrieveToken ($validityTime = 3600) {
        $url = $this->getTokenUrl();

        $params = [
            'validity_time' => $validityTime,
            'grant_type' => 'client_credentials'
        ];

        $content = $this->sendRequest(
            $url,
            'POST',
            http_build_query($params),
            'application/x-www-form-urlencoded',
            $this->getClientAuthCurlOptions(),
            false
        );

        $tokenDetails = json_decode($content);

        if (! property_exists($tokenDetails, 'access_token')) {
            throw new \Exception('No access token was available in response!');
        }

        if (! property_exists($tokenDetails, 'expires_in')) {
            throw new \Exception('No expire time was available with access token!');
        }

        return $tokenDetails;
    }

    /**
     * Send a HTTP request to the given URL using the given method. By default, this will first retrieve an access token
     * if we do not already have one.
     * @param string $url
     * @param string $method GET, POST, PUT, DELETE, etc
     * @param array|string $body Request Body
     * @param string $contentType MIME type to use for the Request Content-Type header. Defaults to multipart/form-data
     * @param null|array $extraCurlOptions
     * @param bool $sendAccessToken
     * @return null|string
     * @throws \Exception
     */
    protected function sendRequest ($url, $method = 'GET', $body = null, $contentType = 'multipart/form-data',
                                    $extraCurlOptions = null, $sendAccessToken = true) {
        $tries = 0;

        do {
            $method = strtoupper($method);

            $headers = $sendAccessToken ? $this->getHeaders() : [];

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            if ($method == 'GET') {
                // Don't do anything special here
            }
            elseif ($method == 'POST') {
                $headers[] = 'Content-Type: ' . $contentType;

                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
            }
            else {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

                if ($body) {
                    $headers[] = 'Content-Type: ' . $contentType;
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
                }
            }

            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            if ($extraCurlOptions) {
                curl_setopt_array($curl, $extraCurlOptions);
            }

            $content = curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $tries++;
        }
        while ($tries < 3 && ($statusCode != 200 || $content === false));

        if ($statusCode == 200 && $content !== false) {
            return $content;
        }
        else {
            throw new \Exception('Oauth2 ' . $method . ' Request to URL ' . $url . ' Failed! Tries = ' . $tries . ' Status Code = ' . $statusCode);
        }
    }

    /**
     * Returns an array of header lines. Eg Authorization: Bearer a7d8fg6gh67r...
     *
     * @return array
     * @throws \Exception
     */
    public function getHeaders () {
        $accessToken = $this->getAccessToken();

        return [
            'Authorization: Bearer ' . $accessToken
        ];
    }

    /**
     * Get the Authorization string needed for Curl.
     * @return string
     */
    protected function getClientAuthString() {
        return $this->getClientId() . ':' . $this->getClientSecret();
    }

    /**
     * Get the Authorization string needed for Curl.
     * @return array
     */
    protected function getClientAuthCurlOptions() {
        return [
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $this->getClientAuthString()
        ];
    }

    /**
     * @return string
     */
    public function getClientId () {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId ($clientId) {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret () {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret ($clientSecret) {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return string
     */
    public function getTokenUrl () {
        return $this->tokenUrl;
    }

    /**
     * @param string $tokenUrl
     */
    public function setTokenUrl ($tokenUrl) {
        $this->tokenUrl = $tokenUrl;
    }
}