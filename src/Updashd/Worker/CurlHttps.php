<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;

class CurlHttps extends AbstractCurlHttpWorker {
    
    /**
     * @return Config
     */
    public static function createConfig () {
        $config = parent::createConfig();
        $config->setDefaultValue(parent::FIELD_PORT, 443);
        $config->setDefaultValue(parent::FIELD_PROTOCOL, 'https');
        return $config;
    }
    
    /**
     * Get the name of the service (this should match in the database)
     * @return string
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public static function getServiceName () {
        return 'curl_https';
    }
    
    /**
     * Get the readable name of the service
     */
    public static function getReadableName () {
        return 'Curl HTTPS';
    }
}